//
// Created by lu on 6/14/15.

#include <malloc.h>
#include "vector.h"

static inline int default_compare_func(void * a, void *b){//wrapper for compare func so we can point to it
    return (a < b);
}

Vector *vector_init(Vector_compare_func compare_func, void *initArray) {
    Vector *vector_ptr = malloc(sizeof(Vector));

    if (vector_ptr){

        vector_ptr->capacity = DEFAULT_CAPACITY;
        vector_ptr->size = 0;
        vector_ptr->compare_func = compare_func != NULL ? compare_func : default_compare_func;
        vector_ptr->data = initArray != NULL ? initArray : malloc(sizeof(void *) * vector_ptr->capacity);

        if (vector_ptr->data){
            return vector_ptr;
        }
    }
    return NULL;//vect was NOT allocated correctly. todo replace with proper error code
}

void vector_append(Vector *vector, void * value) {
    // make sure there's room to expand into
    if (vector->size >= vector->capacity){// if it is full...
        vector_resize(vector, (vector->size) << 1);//double the array size
    }

    // append the value and increment vector->size
    vector->data[vector->size++] = value;
}

void *vector_get(const Vector *vector, const size_t index) {
    if (index < vector->size && index >= 0) {
        return vector->data[index];
    }
    return NULL;
}

void vector_set(Vector *vector, const size_t index, void *value) {
    if (index >= 0 && index < vector->size) {//bounds check
        vector->data[index] = value;
    }
}

void vector_resize(Vector *vector, size_t capacity) {//capacity is new capacity
    void **newData = realloc(vector->data, sizeof(void *) * capacity);
    if (newData){
        vector->data = newData;
        vector->capacity = capacity;
    }
}

void vector_free(Vector *vector) {
    free(vector->data);
    free(vector);
}

/*Success means returning a pointer to deleted index, else return null*/
void vector_delete(Vector *vector, size_t index) {//delete specific item at index, not the vector itself
    if (index < 0 || index > vector->size -1){//bounds check
        vector->data[index] = NULL;

        /* Now move everything right of the index by one.
         * We can modify index arg without worry since this is the last time we use it */
        for (; index < vector->size; index++) {
            vector->data[index] = vector->data[index + 1];//e.g. index 50 now has element at 51
            vector->data[index + 1] = NULL;//51 is now NULL, loop over it and assign 52 item to 51, etc...
        }
        vector->size--;
    }
}

/*Insert item at index, pushing existing item and everything to the right*/
void *vector_insert(Vector *vector, const size_t insertIndex, void *newItem) {
    if (insertIndex < 0) {
        return NULL;//out of bounds
    }
    if (insertIndex >= vector->size){//fill up to new index
        for (int i = 0; i <= insertIndex; ++i) {
            vector_append(vector, NULL);//NULL fill up to index
        }
    }
    if (vector->size >= vector->capacity){//ensure the vector is large enough to hold the new value
        vector_resize(vector, vector->capacity << 1);//bitshift to double size.
    }

    for (size_t i = vector->size; i >= insertIndex; --i) {//push index item and everything right of it right by 1
        vector->data[i + 1] = vector->data[i];
    }
    vector->data[insertIndex] = newItem;

    vector->size++;
    return vector->data[insertIndex];
}

