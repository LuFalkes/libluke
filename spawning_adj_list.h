//
// Created by lu on 9/6/15.
//

#ifndef LIBLUKE_SPAWNING_ADJ_LIST_H
#define LIBLUKE_SPAWNING_ADJ_LIST_H

#include "vector.h"

typedef void * T_PTR;

typedef struct {
    Vector relations;
    T_PTR element;
} AdjListNode;

typedef struct {
    AdjListNode * head;//arbitrary initial item
} AdjList;

/* Initialize a AdjListNode with main element and related elements. If relations is null
 * it will assume there are no related elements.
 * Otherwise it will call the associate function on a newly allocated node */
extern AdjListNode * adjListNode_init(T_PTR element, T_PTR relations);

#endif //LIBLUKE_SPAWNING_ADJ_LIST_H
