//
// Created by lu on 7/5/15.
// binary search tree

#ifndef LIBLUKE_TREE_H
#define LIBLUKE_TREE_H

#include <stddef.h>
#define ITEM_UNFOUND (-1)
#define ITEM_FOUND (1)
#define ITEM_DELETED (2)
#define TRUE (1)
#define FALSE (0)

typedef void * T_PTR;

typedef int (*tree_compare_func)(T_PTR, T_PTR);

typedef struct TreeNode {
    T_PTR element;
    size_t depth;
    struct TreeNode * left;
    struct TreeNode * right;
} TreeNode;

extern int tree_insert(T_PTR value, TreeNode root, tree_compare_func compare_func);

extern unsigned short tree_contains(T_PTR value, TreeNode *root, tree_compare_func);

extern int tree_delete(T_PTR value, TreeNode *root, tree_compare_func);

extern TreeNode * tree_get(T_PTR value, TreeNode *root, tree_compare_func);

extern TreeNode *treeNode_init(T_PTR key, T_PTR data);

extern TreeNode * leftRotate(TreeNode *node);
#endif //LIBLUKE_TREE_H
