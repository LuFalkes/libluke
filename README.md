# README #

libluke is a demonstration of my efforts to teach myself C by implementing fundamental data structures for personal use. 
Some of the code may seem scratchpad-ish or amateur; feel free to let me know if you think so, I am always open to suggestions.

Currently it uses no third party dependencies, only the standard C99 spec features. 