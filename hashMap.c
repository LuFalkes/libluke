//
// Created by lu on 6/14/15.

#include <stddef.h>
#include <malloc.h>
#include <string.h>
#include "hashMap.h"


typedef struct { //element-val pair in bucket container
    T_PTR key;
    T_PTR value;
    size_t hash;//the value returned by hash function
} Bucket;//each node contains a element val pair as well as the hashmap_hash

/*creates hashmap node. the hash should be returned from the hash function.*/
Bucket *bucket_init(const size_t hash, T_PTR key, T_PTR value);

/* Bucket container is a workaround for the issue of collisions.
 * Duplicates and everything are held in the container. 99% of the time will hold 1 item, .1% will hold 2.
 * Anything more is a freak occurence.*/
typedef struct {
    Vector *buckets;
} BucketContainer;

BucketContainer *container_init();

static int default_compare(char * a, char* b ){//wrapper around strcmp so we can use a function pointer to it
    return strcmp(a, b);
}

static size_t hashmap_hash(HashMap *hashMap, T_PTR key) {//bjb2 hash, simple general hash
    int c;
    size_t hash = 0;

    while ((c = *((int *) key++))){
        hash = ((hash << 5) + hash) + c;
    }
    return hash % hashMap->containers->capacity;//index to return
    // ^ mod by capacity of vect to account for overflow, so vect of size 13 with
    // hash 40 will have index of 1 after 3x overflow is accounted for
}

HashMap *hashmap_init(hashmap_compare_func compare, hashmap_hash_func hash) {//compare param is function that is used for comparing the generic type
    HashMap *hashmap_ptr = malloc(sizeof(HashMap));
    if(hashmap_ptr){
        hashmap_ptr->containers = vector_init(NULL, NULL);
        hashmap_ptr->compare_func = compare == NULL ? default_compare : compare; //assign comparison function, default to string compare
        hashmap_ptr->hash_func = hash == NULL ? hashmap_hash : hash;
        return hashmap_ptr;//malloc success
    }
    return NULL;//malloc failure
}

static void rehash(HashMap *hashmap_ptr) {//todo replace new vect creation with realloc
    hashmap_ptr->containers->capacity = hashmap_ptr->containers->capacity << 1;//double capacity
    hashmap_ptr->containers->data = realloc(hashmap_ptr->containers->data,//resize mem address for new capacity
                                        sizeof(void *) * hashmap_ptr->containers->capacity);

    Bucket *pair;//element val pair
    for (size_t k = 0; k < hashmap_ptr->containers->size; ++k) {//for each pair, rehash it and free old
        pair = vector_get(hashmap_ptr->containers, k);//get old element val pair
        hashmap_add(hashmap_ptr, pair->key, pair->value);//hash item again, creating new bucket
        free(pair);//free old element val pair
    }
}

void hashmap_add(HashMap *hashmap, void *key, void *value) {//returns pointer to value location
    //check loadfactor, resize if needed
    if (hashmap->containers->size + 1 >= (hashmap->containers->capacity * LOAD_FACTOR)) {
        rehash(hashmap);
    }

    Bucket *bucket_ptr = bucket_init(hashmap_hash(hashmap, key), key, value);//bucket_ptr for new item
    BucketContainer *container = hashmap_get(hashmap, key);//the container of the existing item to modify...

    if (container) {//if element already exists in map...
        vector_append(container->buckets, bucket_ptr);//add element val pair to existing container
    } else {//if does not exist in map (i.e. is null), add new entry
        container = container_init();
        vector_append(container->buckets, bucket_ptr);
        vector_insert(hashmap->containers, bucket_ptr->hash, container);
    }
    hashmap->containers->size++;
}

void *hashmap_get(HashMap *hashmap, void *key) {
    BucketContainer *bucketContainer = vector_get(hashmap->containers, (hashmap_hash(hashmap, key)));
    Bucket *pair;

    if (bucketContainer) {
        for (size_t j = 0; j < bucketContainer->buckets->size; ++j) {//each container should rarely contain > 1
            if ((pair = vector_get(bucketContainer->buckets, j))){
                if (hashmap->compare_func(key, pair->key) == 0) {
                    return pair->value;
                }
            }
        }
    }
    return NULL;//if bucket container does not exist
}


Bucket *bucket_init(const unsigned long hash, void *key, void *value) {
    Bucket *hashmapnode = malloc(sizeof(Bucket));

    hashmapnode->hash = hash;
    hashmapnode->key = key;
    hashmapnode->value = value;
    return hashmapnode;
}

void hashmap_set(HashMap *hashmap_ptr, void *key, void *newValue) {
    Bucket *bucket = hashmap_get(hashmap_ptr, key);
    bucket->value = newValue;
}

unsigned short int hashmap_contains(HashMap *hashmap, void *key) {
    size_t index = hashmap_hash(hashmap, key);
    if (vector_get(hashmap->containers, index) != NULL) {
        return 1;//true
    }
    return 0;//false
}

void hashmap_remove(HashMap *map_ptr, void *key) {
    free(hashmap_get(map_ptr, key));//frees a bucket struct
}

void hashmap_free(HashMap *map_ptr) {
    Bucket *bucket_ptr;
    BucketContainer *container_ptr;
    for (size_t i = 0; i < map_ptr->containers->size; ++i) {
        container_ptr = vector_get(map_ptr->containers, i);
        for (size_t j = 0; j < container_ptr->buckets->size; ++j) {
            bucket_ptr = vector_get(container_ptr->buckets, j);
            free(bucket_ptr);
        }
        free(container_ptr);
    }
    free(map_ptr);
}

BucketContainer *container_init() {
    BucketContainer *container =  malloc(sizeof(BucketContainer));
    if (container){
        container->buckets = vector_init(NULL, NULL);
        if (container->buckets){
            return container;
        }
        free(&container->buckets);//malloc failure
    }
    free(&container);//malloc failure
    return NULL;
}


int hashmap_foreach(HashMap *hashMap_ptr, F function, T_PTR firstArgOfFunc) {
    Bucket * bucket_ptr;
    BucketContainer * bucketContainer_ptr;

    if (hashMap_ptr->containers->size <= 0){
        return 0;//empty map, error out
    }

    for (size_t i = 0; i < hashMap_ptr->containers->size; ++i) {
        bucketContainer_ptr = vector_get(hashMap_ptr->containers, ++i);
        for (size_t j = 0; j < bucketContainer_ptr->buckets->size; ++j) {
            bucket_ptr = vector_get(bucketContainer_ptr->buckets, j);
            int status_code =  function(firstArgOfFunc, bucket_ptr->value);
            if (status_code < 1){
                return status_code;//determined by func passed as arg
            }
        }
    }
    return 0;
}
