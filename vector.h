// Created by lu on 6/14/15.

#ifndef LIBLUKE_VECTOR_H
#define LIBLUKE_VECTOR_H

#define DEFAULT_CAPACITY 255
typedef int (*Vector_compare_func)(void *a, void *b);//pointer to comparison func

typedef struct {
    void **data;     // pointer to array of values
    size_t size;      // slots used so far
    size_t capacity;  // total available slots
    Vector_compare_func compare_func;
} Vector;

/* Constructs vector. Init array is an array that can be wrapped around with a vector. If null then uses default. */
Vector *vector_init(Vector_compare_func compare_func, void *initArray);

/* Appends item to end of vector*/
extern void vector_append(Vector *vector, void * value);

/* Retrieves pointer to item at vector index*/
extern void *vector_get(const Vector *vector, const size_t index);

/* Set returns pointer to newly changed item. on failure to set it returns null */
extern void vector_set(Vector *vector, const size_t index, void *value);

/* Increase vector to size of specified capacity*/
extern void vector_resize(Vector *vector, size_t capacity);

/* Destructs vector*/
extern void vector_free(Vector *vector);

/* Deletes item at index, and shifts all items left*/
extern void vector_delete(Vector *vector, size_t);

/* Insert item at index, shifting the previous item and company to the right.
 * Returns pointer to new item on success */
extern void *vector_insert(Vector *vector, const size_t insertIndex, void *newItem);

#endif //LIBLUKE_VECTOR_H
