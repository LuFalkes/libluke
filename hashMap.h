//
// Created by lu on 6/14/15.
// todo implement re hashing when ever we change buckets vector size

#ifndef LIBUKE_HASHMAP_H
#define LIBUKE_HASHMAP_H
#include <stdint.h>
#include <unistring/stdint.h>
#include "vector.h"

 /*Load factor determines at what percentage of capacity the map should rehash. Remember, hashed items are more
  * performant and better to work with if they have a more even spread. */
#define LOAD_FACTOR .75f

typedef void * T_PTR;//pointer to type T.

/* Below is an incomplete declaration to prototype for hashmap_hash_func */
struct Hashmap;

/* pointer to function to use to compare for equality, defaults to strcmp  */
typedef int (*hashmap_compare_func)(T_PTR a, T_PTR b);
/* pointer to hashing function, defaults to djb2 hash */
typedef size_t (*hashmap_hash_func)(struct Hashmap,  T_PTR key);

/*Pointer to some yet unknown generic function. Used for iteration/mapping*/
typedef int (*F)(T_PTR, T_PTR);

typedef struct {
    Vector *containers;//pointer to vector of bucket containers that contain pairs
    hashmap_compare_func compare_func;//function to use for comparison
    hashmap_hash_func hash_func;
    //size and capacity information exist as members of Vector *containers;
} HashMap;//the map consists of a vector of nodes

/*Allocate map items*/
extern HashMap *hashmap_init(hashmap_compare_func compare, hashmap_hash_func hash);

/*Destroys hashmap*/
extern void hashmap_free(HashMap *map_ptr);

/* Enters a element val pair. if item exists already or there
 * is a collision it adds a new pair to the container .*/
extern void hashmap_add(HashMap *hashmap_ptr, T_PTR key, T_PTR value);

/*gets val associated with element*/
extern void *hashmap_get(HashMap *hashmap_ptr, T_PTR key);

/*Set existing value associated with element  to new value*/
extern void hashmap_set(HashMap *hashmap_ptr, T_PTR key, T_PTR newValue);

/*removes element pair*/
extern void hashmap_remove(HashMap *map_ptr, T_PTR key);

/*returns 0 if element  is not in map, else return 1 */
extern unsigned short int hashmap_contains(HashMap *hashmap_ptr, T_PTR key);

/*applies a function on each item in map. Returns 0 on failure. */
extern int hashmap_foreach(HashMap *hashMap_ptr, F function, T_PTR firstArgOfFunc);

#endif //LIBUKE_HASHMAP_H
