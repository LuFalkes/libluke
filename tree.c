//
// Created by lu on 7/5/15.
//
#include <malloc.h>
#include "tree.h"


TreeNode *treeNode_init(T_PTR key, T_PTR data) {
    TreeNode * node_ptr = malloc(sizeof(TreeNode));

    if (node_ptr) {
        node_ptr->depth = 0;
        node_ptr->element = key;
        node_ptr->element = data;
        node_ptr->left = NULL;
        node_ptr->right = NULL;
        return node_ptr;
    }

    return NULL;//malloc failure
}

int tree_delete(T_PTR key, TreeNode *root, tree_compare_func func) {
    TreeNode * node_ptr = tree_get(key, root, func);
    if (node_ptr){
        free(node_ptr);
        return ITEM_DELETED;
    }
    return ITEM_UNFOUND;
}

TreeNode *tree_get(T_PTR key, TreeNode *root, tree_compare_func func) {
    switch (func(root->element, key)){
        case 0:
            return root;
        case -1:
            tree_get(key, root->left, func);
        case 1:
            tree_get(key, root->right, func);
        default:break;
    }
    return NULL;//item could not be found, or null node
}

unsigned short tree_contains(T_PTR value, TreeNode *root, tree_compare_func func) {
    if (tree_get(value, root, func)){
        return TRUE;
    }
    return FALSE;
}

TreeNode *leftRotate(TreeNode *node) {
    TreeNode * right_ptr = node->right;
    TreeNode * left_ptr = right_ptr->left;

    right_ptr->left = node;
    node->right = left_ptr;

    node->depth = (node->left->depth > node->right->depth ? node->left->depth : node->right->depth) + 1;
    right_ptr->depth =
            (right_ptr->left->depth > right_ptr->right->depth ? right_ptr->left->depth : right_ptr->right->depth ) + 1;

    return right_ptr;
}
